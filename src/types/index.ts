export interface Fact {
    id?: number,
    name_id?: number,
    type_id?: number,
    candy_id?: number,
    candy_count_id?: number,
    egg_id?: number,
    weight?: number
}

export interface Pokemon {
    id: number,
    num: string,
    name: string,
    img?: string,
    type: Array<string>,
    height?: string,
    weight?: string,
    candy?: string,
    candy_count?: number,
    egg?: string,
    spawn_chance?: number,
    avg_spawns?: number,
    spawn_time?: string,
    multipliers?: Array<number>,
    weaknesses?: Array<string>,
    next_evolution?: Array<Evolution>,
    prev_evolution?: Array<Evolution>
}

export interface Evolution {
    num: string,
    name: string
}

export interface DimensionTable {
    dimensionTable: Dimension,
    fields?: Array<any>,
    level?: Array<DimensionTable>
}
export interface Dimension {
    dimension: string,
    keyProps: Array<string>,
    otherProps?: Array<string>,
}

export interface DimensionWrapper {
    name: string,
    table: Array<any>,
    fields: Array<string>,
    level?: Array<DimensionTable>
}

export interface SortOption {
    text: any;
    value: any;
}

export interface Dice {
    name: string,
    ids: Array<number>
}

export interface RootResponse {
    facts: Array<Pokemon>,
    cells: Array<any>
}

export interface SelectValue {
    name: string,
    tables: Array<any>,
    destroy?: Boolean
}
