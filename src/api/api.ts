import * as Type from '@/types/index';

const baseUrl = 'http://localhost:3000';


export const getRawData = async (): Promise<Array<Type.Pokemon>> => {
    return new Promise(async (resolve, reject) => {
        try {
            const res = await fetch(`${baseUrl}/raw`, {
                method: 'GET'
            })
            const json = await res.json();
            resolve(json);
        } catch (error) {
            reject(error);
        }
    })
}

export const getFacts = async (): Promise<Array<Type.Fact>> => {
    return new Promise(async (resolve: any, reject: any) => {
        try {
            const res = await fetch(`${baseUrl}/facts`, {
                method: 'GET'
            })
            const json = await res.json();
            resolve(json);
        } catch (error) {
            reject(error);
        }
    })
}

export const getDimensions = async (): Promise<Array<Type.DimensionWrapper>> => {
    return new Promise(async (resolve, reject) => {
        try {
            const res = await fetch(`${baseUrl}/dimensions`, {
                method: 'GET'
            })
            const json = await res.json();
            resolve(json);
        } catch (error) {
            reject(error);
        }
    })
}

export const getDice = async (data: Array<Type.Dice>): Promise<Type.RootResponse> => {
    return new Promise(async (resolve, reject) => {
        try {
            const res = await fetch(`${baseUrl}/dice`, {
                method: 'POST',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json'
                },
            })
            const json = await res.json();
            resolve(json);
        } catch (error) {
            reject(error);
        }
    })
}