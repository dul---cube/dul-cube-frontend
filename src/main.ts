import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
require('vue-ionicons/ionicons.css')
import router from './router';
import App from './App.vue';


const zingchartVue = require('zingchart-vue');

Vue.component('zingchart', zingchartVue)


import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import '@/assets/main.css';


Vue.config.productionTip = false;
Vue.use(BootstrapVue);

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
