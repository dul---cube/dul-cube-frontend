import Vue from 'vue';
import VueRouter, { Route } from 'vue-router';
import Overview from '../views/Overview.vue';
import Dice from '../views/Dice.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'overview',
    meta: {
      title: 'Overview',
    },
    component: Overview,
  },
  {
    path: '/dice',
    name: 'dice',
    meta: {
      title: 'Dice',
    },
    component: Dice,
  },
];


const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});


router.beforeEach((to: Route, from: Route, next: Function) => {
  document.title = to.meta.title
  next()
})

export default router;
